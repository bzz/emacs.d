.PHONY: default update lines help

default: update

update: ## Reset init.el, use init.org on EMACS restart
	rm -rf init.el init.elc
	cp -f _init.el init.el

lines:  ## Count lines
	wc -l init.org

help:   ## List rules
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk \
	'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
