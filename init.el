;;; -*- lexical-binding: t -*-

(defun tangle-init ()
  "If the current buffer is 'init.org' the code-blocks are
tangled, and the tangled file is compiled."
  (when (equal (buffer-file-name)
               (expand-file-name (concat user-emacs-directory "init.org")))
    ;; Avoid running hooks when tangling.
    (let ((prog-mode-hook nil))
      (org-babel-tangle)
      (byte-compile-file (concat user-emacs-directory "init.el")))))

(add-hook 'after-save-hook 'tangle-init)

(eval-when-compile
  (setq use-package-expand-minimally byte-compile-current-file))

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                 (not (gnutls-available-p))))
    (proto (if no-ssl "http" "https")))
    ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
    (add-to-list 'package-archives (cons "melpa-unstable" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
    (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
(add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))

(setq use-package-always-ensure t)

(use-package system-packages)
(use-package use-package-ensure-system-package)

(defun my/emacs-path (path)
  "Expands `path` with Emacs home directory."
  (expand-file-name path user-emacs-directory))

(defun my/tmp-path (path)
  "Expand `path` with Emacs temporary directory."
  (my/emacs-path (format "tmp/%s" path)))

(defun my/lisp-path (path)
  "Expand `path` with Emacs `/lisp` directory."
  (my/emacs-path (format "lisp/%s" path)))

(setq mac-right-command-modifier 'super)
(setq mac-option-modifier 'meta)
(setq mac-left-option-modifier 'meta)
(setq mac-right-option-modifier 'meta)
(setq mac-command-modifier 'super)

(setq mac-right-option-modifier 'nil)

(load-theme 'deeper-blue)
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark))

(set-cursor-color "lightsalmon1")
(set-mouse-color "white")

(set-face-attribute 'default nil :font "Source Code Pro 11")
;; (setq default-frame-alist '((font . "Inconsolata LGC 14")))
(setq-default line-spacing 0)
(setq initial-frame-alist '((width . 135) (height . 55)))
(tool-bar-mode -1)

(set-face-background 'show-paren-match "salmon3")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)
(show-paren-mode)

;; (global-visual-line-mode 1)
(setq org-startup-truncated nil)

(setq column-number-mode t) ;; show columns in addition to rows in mode line

(setq-default frame-title-format "%b (%f)")

(setq-default indent-tabs-mode nil)
(setq tab-width 2)
(setq js-indent-level 2)
(setq css-indent-offset 2)
(setq-default c-basic-offset 2)
(setq c-basic-offset 2)
(setq-default tab-width 2)
(setq-default c-basic-indent 2)

(use-package frame
 :ensure nil
 :config
 (blink-cursor-mode 0)
 :bind
 ("C-z" . nil))

(use-package delsel
 :ensure nil
 :bind
 ("C-c C-g" . minibuffer-keyboard-quit))

(global-visual-line-mode t)

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil)  ; stop creating .# files

(setq ring-bell-function 'ignore)  ; turn off alarms completely

(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

(setq
 use-dialog-box nil       ; Disable dialog boxes
 use-file-dialog nil)     ; Disable file dialog

(setq
 scroll-margin 0
 scroll-conservatively 0
 scroll-preserve-screen-position nil
 scroll-step 1)

(tool-bar-mode -1)
(scroll-bar-mode -1)
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))

(if (eq system-type 'darwin)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (set-frame-parameter frame 'menu-bar-lines
                                     (if (display-graphic-p frame) 1 0))))
  (when (fboundp 'menu-bar-mode)
    (menu-bar-mode -1)))

(setq-default bidi-display-reordering nil)

(setq-default blink-matching-paren nil)

(setq-default cursor-in-non-selected-windows nil)

(setq-default
 cursor-type 'bar
 x-stretch-cursor t)

(setq-default frame-title-format "%b (%f)")

(setq-default frame-inhibit-implied-resize t)

(when (boundp 'window-divider-mode)
  (setq window-divider-default-places t
        window-divider-default-bottom-width 0
        window-divider-default-right-width 0)
  (window-divider-mode +1))

(add-hook
 'term-mode-hook
 (lambda () (setq line-spacing 0)))

(setq show-paren-style 'parenthesis)
(show-paren-mode 1)

(setq delete-selection-mode t)

(setq-default
 left-margin-width 2
 right-margin-width 2)

(use-package time
 :ensure nil
 :custom
 (display-time-default-load-average nil)
 (display-time-24hr-format t)
 :config
 (display-time-mode t))

(setq byte-compile-warnings '(not obsolete))

(eval-when-compile
  (setq use-package-expand-minimally byte-compile-current-file))

(setq ad-redefinition-action 'accept)

(global-auto-revert-mode t)

(setq
 inhibit-startup-message t         ; Don't show the startup message
 inhibit-startup-screen t          ; or screen
 cursor-in-non-selected-windows t  ; Hide the cursor in inactive windows

 echo-keystrokes 0.1               ; Show keystrokes right away, don't show the message in the scratch buffer
 initial-scratch-message nil       ; Empty scratch buffer
 sentence-end-double-space nil     ; Sentences should end in one space, come on!
)

(fset 'yes-or-no-p 'y-or-n-p)      ; y and n instead of yes and no everywhere else
(scroll-bar-mode -1)
(delete-selection-mode 1)
(global-unset-key (kbd "s-p"))

(use-package simpleclip
  :config
  (simpleclip-mode 1))

(use-package exec-path-from-shell)

(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

(use-package shell-pop)

(global-set-key (kbd "s-<backspace>") 'kill-whole-line)
(global-set-key (kbd "M-S-<backspace>") 'kill-word)

(global-set-key (kbd "s-<right>") 'end-of-visual-line)
(global-set-key (kbd "s-<left>") 'beginning-of-visual-line)
;; (global-set-key (kbd "S-s-<left>") (kbd "M-S-m"))

(global-set-key (kbd "s-<up>") 'beginning-of-buffer)
(global-set-key (kbd "s-<down>") 'end-of-buffer)

(global-set-key (kbd "s-l") 'goto-line)

(global-set-key (kbd "s-a") 'mark-whole-buffer)       ;; select all
(global-set-key (kbd "s-s") 'save-buffer)             ;; save
(global-set-key (kbd "s-S") 'write-file)              ;; save as
(global-set-key (kbd "s-q") 'save-buffers-kill-emacs) ;; quit
(global-set-key (kbd "s-z") 'undo)                    ;; undo

(use-package avy
  :config
  (global-set-key (kbd "s-;") 'avy-goto-char-timer))

(defun my-pop-local-mark-ring ()
  (interactive)
  (set-mark-command t))

(defun unpop-to-mark-command ()
  "Unpop off mark ring. Does nothing if mark ring is empty."
  (interactive)
      (when mark-ring
        (setq mark-ring (cons (copy-marker (mark-marker)) mark-ring))
        (set-marker (mark-marker) (car (last mark-ring)) (current-buffer))
        (when (null (mark t)) (ding))
        (setq mark-ring (nbutlast mark-ring))
        (goto-char (marker-position (car (last mark-ring))))))

(global-set-key (kbd "s-,") 'my-pop-local-mark-ring)
(global-set-key (kbd "s-.") 'unpop-to-mark-command)

(global-set-key (kbd "s-<") 'previous-buffer)
(global-set-key (kbd "s->") 'next-buffer)

(defun vsplit-last-buffer ()
  (interactive)
  (split-window-vertically)
  (other-window 1 nil)
  (switch-to-next-buffer))

(defun hsplit-last-buffer ()
  (interactive)
  (split-window-horizontally)
  (other-window 1 nil)
  (switch-to-next-buffer))

(global-set-key (kbd "s-w") (kbd "C-x 0")) ;; just like close tab in a web browser
(global-set-key (kbd "s-W") (kbd "C-x 1")) ;; close others with shift

(global-set-key (kbd "s-T") 'vsplit-last-buffer)
(global-set-key (kbd "s-t") 'hsplit-last-buffer)

(use-package expand-region
  :config
  (global-set-key (kbd "s-'") 'er/expand-region)
  (global-set-key (kbd "s-\"") 'er/contract-region))

(use-package move-text
  :config
  (move-text-default-bindings))

(defun smart-open-line ()
  "Insert an empty line after the current line. Position the cursor at its beginning, according to the current mode."
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent))

(defun smart-open-line-above ()
  "Insert an empty line above the current line. Position the cursor at it's beginning, according to the current mode."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key (kbd "s-<return>") 'smart-open-line)
(global-set-key (kbd "s-S-<return>") 'smart-open-line-above)

(defun smart-join-line (beg end)
  "If in a region, join all the lines in it. If not, join the current line with the next line."
  (interactive "r")
  (if mark-active
      (join-region beg end)
      (top-join-line)))

(defun top-join-line ()
  "Join the current line with the next line."
  (interactive)
  (delete-indentation 1))

(defun join-region (beg end)
  "Join all the lines in the region."
  (interactive "r")
  (if mark-active
      (let ((beg (region-beginning))
            (end (copy-marker (region-end))))
        (goto-char beg)
        (while (< (point) end)
          (join-line 1)))))

(global-set-key (kbd "s-j") 'smart-join-line)

(global-set-key (kbd "M-u") 'upcase-dwim)
(global-set-key (kbd "M-l") 'downcase-dwim)

(use-package visual-regexp
  :config
  (define-key global-map (kbd "s-r") 'vr/replace))

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq require-final-newline t)

(use-package multiple-cursors
  :config
  (setq mc/always-run-for-all 1)
  (global-set-key (kbd "s-d") 'mc/mark-next-like-this)
  (global-set-key (kbd "s-D") 'mc/mark-all-dwim)
  (define-key mc/keymap (kbd "<return>") nil))

(global-set-key (kbd "s-/") 'comment-line)

(setq split-height-threshold 0)
(setq split-width-threshold nil)

(global-set-key (kbd "s-o") (kbd "C-x o"))

(use-package windmove
  :config
  (global-set-key (kbd "s-[")  'windmove-left)         ;; Cmd+[ go to left window
  (global-set-key (kbd "s-]")  'windmove-right)        ;; Cmd+] go to right window
  (global-set-key (kbd "s-{")  'windmove-up)           ;; Cmd+Shift+[ go to upper window
  (global-set-key (kbd "s-}")  'windmove-down))      ;; Ctrl+Shift+[ go to down window

(winner-mode 1)

(use-package shackle
  :init
  (setq shackle-default-alignment 'below
        shackle-default-size 0.4
        shackle-rules '((help-mode           :align below :select t)
                        (helpful-mode        :align below)
                        (compilation-mode    :select t   :size 0.25)
                        ("*compilation*"     :select nil :size 0.25)
                        ("*ag search*"       :select nil :size 0.25)
                        ("*Flycheck errors*" :select nil :size 0.25)
                        ("*Warnings*"        :select nil :size 0.25)
                        ("*Error*"           :select nil :size 0.25)
                        ("*Org Links*"       :select nil :size 0.1)
                        (magit-status-mode                :align bottom :size 0.5  :inhibit-window-quit t)
                        (magit-log-mode                   :same t                  :inhibit-window-quit t)
                        (magit-commit-mode                :ignore t)
                        (magit-diff-mode     :select nil  :align left   :size 0.5)
                        (git-commit-mode                  :same t)
                        (vc-annotate-mode                 :same t)
                        ))
  :config
  (shackle-mode 1))

(use-package edit-indirect)

(use-package magit
  :config
  (global-set-key (kbd "s-g") 'magit-status))

(setq magit-repository-directories '(("\~/code" . 4) ("\~/Google Drive/Codexpanse/Course materials" . 3)))

(defun magit-status-with-prefix-arg ()
  "Call `magit-status` with a prefix."
  (interactive)
  (let ((current-prefix-arg '(4)))
    (call-interactively #'magit-status)))

(global-set-key (kbd "s-P") 'magit-status-with-prefix-arg)

(use-package company
  :config
  (setq company-idle-delay 0.1)
  (setq company-global-modes '(not org-mode markdown-mode))
  (setq company-minimum-prefix-length 1)
  (add-hook 'after-init-hook 'global-company-mode))

(setq ns-pop-up-frames nil)

(defun kill-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))

(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)

(use-package files
 :ensure nil
 :commands
 (generate-new-buffer
  executable-find
  file-name-base
  file-name-extension)
 :custom
 (require-final-newline t)
 :hook
 (before-save . delete-trailing-whitespace))

(use-package autorevert
 :ensure nil
 :custom
 ;; Don't generate any messages whenever a buffer is reverted
 (auto-revert-verbose nil)
 ;; Operate only on file-visiting buffers
 (global-auto-revert-non-file-buffers t)
 :diminish auto-revert-mode)

(setq
 save-place-forget-unreadable-files t
 save-place-limit 200)

(save-place-mode 1)

(global-set-key (kbd "<C-tab>") 'other-window)

(define-key dired-mode-map [mouse-1] 'dired-find-file)

(if (require 'quelpa nil t)
  ;; Prevent quelpa from doing anyting that requires network connection.
  (setq
   quelpa-update-melpa-p nil    ; Don't update MELPA git repo
   quelpa-checkout-melpa-p nil  ; Don't clone MELPA git repo
   quelpa-upgrade-p nil         ; Don't try to update packages automatically
   quelpa-self-upgrade-p nil)   ; Don't upgrade quelpa automatically

  ;; Comment/uncomment line below to disable/enable quelpa auto-upgrade.
  ;; (quelpa-self-upgrade)

  (with-temp-buffer
    (url-insert-file-contents "https://raw.github.com/quelpa/quelpa/master/bootstrap.el")
    (eval-buffer)))

(quelpa
 '(quelpa-use-package
   :fetcher github
   :repo "quelpa/quelpa-use-package"))
(require 'quelpa-use-package)

(quelpa-use-package-activate-advice)

(setq org-support-shift-select t)

(use-package flycheck
  :after (general)
  :demand t
  :commands
  (global-flycheck-mode)
  :init
  (setq-default
   flycheck-disabled-checkers
   '(emacs-lisp-checkdoc
     javascript-jshint
     haskell-stack-ghc
     haskell-ghc
     haskell-hlint))
  (setq
   flycheck-highlighting-mode 'lines
   flycheck-indication-mode 'left-fringe
   flycheck-mode-line-prefix "fly"
   flycheck-javascript-eslint-executable "eslint_d")
  :config
  (global-flycheck-mode 1)
  ;; Make the error list display like similar lists in contemporary IDEs
  ;; like VisualStudio, Eclipse, etc.
  (add-to-list
   'display-buffer-alist
   `(,(rx bos "*errors*" eos)
     ;; (display-buffer-reuse-window
     ;;  display-buffer-in-side-window)
     (side . bottom)
     (reusable-frames . visible)
     (window-height . 0.33)))
  (unbind-key "C-j" flycheck-error-list-mode-map)
  :diminish flycheck-mode)

(use-package lisp-mode
  :ensure nil
  :config
  (put 'use-package 'lisp-indent-function 1)
  (put 'add-hook 'lisp-indent-function 1))

(use-package elisp-mode
 :after (general company smartparens)
 :ensure nil
 :config
 (add-to-list 'company-backends 'company-elisp)
 (sp-with-modes 'emacs-lisp-mode
   (sp-local-pair "'" nil :actions nil)))

(use-package nix-mode
  :after (general)
  :mode ("\\.nix\\'" "\\.nix.in\\'")
  :config
  :delight "nix")

(use-package nix-drv-mode
  :ensure nix-mode
  :mode "\\.drv\\'")

(use-package nix-repl
  :ensure nix-mode
  :commands (nix-repl))

(use-package company-nixos-options
  :after (company)
  :commands (company-nixos-options)
  :config
  (add-to-list 'company-backends 'company-nixos-options))

(use-package nix-sandbox)

(defconst my/merlin-site-elisp (getenv "MERLIN_SITE_LISP"))
(defconst my/utop-site-elisp (getenv "UTOP_SITE_LISP"))
(defconst my/ocp-site-elisp (getenv "OCP_INDENT_SITE_LISP"))
(defun my/ocaml-in-emacs-p ()
  (string-equal (getenv "OCAML_IN_EMACS") "1"))

(use-package tuareg
  :demand t
  :mode
  (("\\.ml[ily]?$" . tuareg-mode)
   ("\\.topml$" . tuareg-mode))
  :init
  (setq tuareg-match-patterns-aligned t)
  :hook
  (caml-mode . tuareg-mode)
  :delight "ocaml")

(use-package general)

(use-package smartparens
 :commands
 (smartparens-mode
  sp-with-modes
  sp-local-pair
  sp-pair)
 :hook
 ((conf-mode text-mode prog-mode) . smartparens-mode)
 :config
 (use-package smartparens-config
  :ensure nil
  :demand t)
 (general-define-key
 :diminish smartparens-mode))

(use-package merlin
  :after (company tuareg)
  :if (and my/merlin-site-elisp (my/ocaml-in-emacs-p))
  :load-path my/merlin-site-elisp
  :init
  ;; Disable merlin's own error checking
  ;; We'll use flycheck-ocaml for that
  (setq
   merlin-error-after-save nil
   merlin-completion-with-doc t
   merlin-command "/run/current-system/sw/bin/ocamlmerlin")
  :config
  (add-to-list 'company-backends 'merlin-company-backend)
  :hook
  ((tuareg-mode caml-mode) . merlin-mode))

(use-package merlin-eldoc
 :after (merlin)
 :custom
 (eldoc-echo-area-use-multiline-p t)   ; Use multiple lines when necessary
 (merlin-eldoc-max-lines 8)            ; But not more than 8
 (merlin-eldoc-type-verbosity 'min)    ; Don't display verbose types
 (merlin-eldoc-function-arguments nil) ; Don't show function arguments
 (merlin-eldoc-doc nil)                ; Don't show the documentation
 :config
 :hook
 ((tuareg-mode reason-mode) . merlin-eldoc-setup))

(use-package utop
  :after (tuareg)
  :if (and my/utop-site-elisp (my/ocaml-in-emacs-p))
  :load-path my/utop-site-elisp
  :commands
  (utop-command)
  :config
  (setq utop-command "opam config exec utop -- -emacs")
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
  :hook
  ((tuareg-mode reason-mode) . utop-minor-mode))

(use-package ocp-indent
  :after (tuareg)
  :if (and my/ocp-site-elisp (my/ocaml-in-emacs-p))
  :load-path my/ocp-site-elisp
  :config)

(use-package dune)

(use-package flycheck-ocaml
 :after (flycheck merlin)
 :demand t
 :commands
 (flycheck-ocaml-setup)
 :config
 ;; Enable flycheck checker
 (flycheck-ocaml-setup))

(use-package reason-mode
 :quelpa
 (reason-mode :repo "reasonml-editor/reason-mode" :fetcher github :stable t)
 :config
 (add-hook
  'reason-mode-hook
  (lambda ()
    (setq utop-command "opam config exec -- rtop -emacs")
    (add-hook 'before-save-hook 'refmt-before-save)))
    (add-hook 'reason-mode-hook 'merlin-mode)
    (add-hook 'reason-mode-hook 'utop-minor-mode)
    (add-hook 'reason-mode-hook 'flycheck-mode)
    :delight "re")

(use-package rust-mode
 :after (company general)
 :if (executable-find "rustc")
 :commands (rust-mode)
 :config
 ;; enable rust-mode for .lalrpop files
 (add-to-list 'auto-mode-alist '("\\.lalrpop\\'" . rust-mode))
 (general-define-key
  :keymaps 'rust-mode-map
  "TAB" 'company-indent-or-complete-common
  "C-c C-b" 'rust-compile
  "C-c <tab>" 'rust-format-buffer))

(use-package racer
 :after rust-mode
 :demand t
 :commands racer-mode
 :hook
 ((racer-mode . eldoc-mode)
  (racer-mode . company-mode)
  (rust-mode . racer-mode)))

(use-package company-racer
 :after (racer company)
 :config
 (add-to-list 'company-backends 'company-racer))

(use-package flycheck-rust
  :config
  (with-eval-after-load 'rust-mode
    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)))
