#+TITLE: Emacs configuration file
#+AUTHOR: Michael Baynov
#+BABEL: :cache yes
#+PROPERTY: header-args :tangle yes
#+STARTUP: overview

* Credits
Inspired by
https://github.com/freetonik/emacs-dotfiles
https://github.com/vyorkin/emacs.d/


* Setup

- Symlink =init.org= to =~/.emacs.d/init.org=
- Symlink =init.el= to =~/.emacs.d/init.el=

#+BEGIN_SRC sh :tangle no
ln -sf $(pwd)/init.org ~/.emacs.d/init.org
ln -sf $(pwd)/init.el ~/.emacs.d/init.el
#+END_SRC

On the first run Emacs will install some packages. It's best to restart Emacs after that process is done for the first time.

There is no reason to track the =init.el= that is generated; by running the following command =git= will not bother tracking it:

#+BEGIN_SRC sh :tangle no
git update-index --assume-unchanged init.el
#+END_SRC

If one wishes to make changes to the repo-version of =init.el= start tracking again with:

#+BEGIN_SRC sh :tangle no
git update-index --no-assume-unchanged init.el
#+END_SRC

When this configuration is loaded for the first time, the =init.el= is the file that is loaded. It looks like this:

#+BEGIN_SRC emacs-lisp :tangle no
;; We can't tangle without org!
(require 'org)
;; Open the configuration
(find-file (concat user-emacs-directory "init.org"))
;; tangle it
(org-babel-tangle)
;; load it
(load-file (concat user-emacs-directory "init.el"))
;; finally byte-compile it
(byte-compile-file (concat user-emacs-directory "init.el"))
#+END_SRC


* Configurations

** Meta

When this configuration is loaded for the first time, the =init.el= is the file that is loaded. It looks like this:

#+BEGIN_SRC emacs-lisp :tangle no
;; This file replaces itself with the actual configuration at first run.

;; We can't tangle without org!
(require 'org)
;; Open the configuration
(find-file (concat user-emacs-directory "init.org"))
;; tangle it
(org-babel-tangle)
;; load it
(load-file (concat user-emacs-directory "init.el"))
;; finally byte-compile it
(byte-compile-file (concat user-emacs-directory "init.el"))
#+END_SRC

Lexical scoping for the init-file is needed, it can be specified in the header. This is the first line of the actual configuration:

#+BEGIN_SRC emacs-lisp
;;; -*- lexical-binding: t -*-
#+END_SRC

Tangle and compile this file on save automatically:

#+BEGIN_SRC emacs-lisp
(defun tangle-init ()
  "If the current buffer is 'init.org' the code-blocks are
tangled, and the tangled file is compiled."
  (when (equal (buffer-file-name)
               (expand-file-name (concat user-emacs-directory "init.org")))
    ;; Avoid running hooks when tangling.
    (let ((prog-mode-hook nil))
      (org-babel-tangle)
      (byte-compile-file (concat user-emacs-directory "init.el")))))

(add-hook 'after-save-hook 'tangle-init)
#+END_SRC

This helps get rid of =functions might not be defined at runtime= warnings. See https://github.com/jwiegley/use-package/issues/590

#+BEGIN_SRC emacs-lisp
(eval-when-compile
  (setq use-package-expand-minimally byte-compile-current-file))
#+END_SRC

** Use package

Initialize package and add Melpa source.

#+BEGIN_SRC emacs-lisp
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                 (not (gnutls-available-p))))
    (proto (if no-ssl "http" "https")))
    ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
    (add-to-list 'package-archives (cons "melpa-unstable" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
    (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
(add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)
#+END_SRC

Install use-package.

#+BEGIN_SRC emacs-lisp
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))

(setq use-package-always-ensure t)
#+END_SRC

Ensure system binaries exist alongside package declarations. It uses the
[[https://gitlab.com/jabranham/system-packages][system-packages]] to make handling installed system packages more convenient
(supports =nix= and many other operating systems).

#+BEGIN_SRC emacs-lisp
(use-package system-packages)
(use-package use-package-ensure-system-package)
#+END_SRC

** Path

#+BEGIN_SRC emacs-lisp
(defun my/emacs-path (path)
  "Expands `path` with Emacs home directory."
  (expand-file-name path user-emacs-directory))

(defun my/tmp-path (path)
  "Expand `path` with Emacs temporary directory."
  (my/emacs-path (format "tmp/%s" path)))

(defun my/lisp-path (path)
  "Expand `path` with Emacs `/lisp` directory."
  (my/emacs-path (format "lisp/%s" path)))
#+END_SRC

** Modifier keys

Emacs control is Ctrl. Emacs Super is Command. Emacs Meta is Alt.

#+BEGIN_SRC emacs-lisp
(setq mac-right-command-modifier 'super)
(setq mac-option-modifier 'meta)
(setq mac-left-option-modifier 'meta)
(setq mac-right-option-modifier 'meta)
(setq mac-command-modifier 'super)
#+END_SRC

Right Alt (option) can be used to enter symbols like em dashes =—=.

#+BEGIN_SRC emacs-lisp
(setq mac-right-option-modifier 'nil)
#+END_SRC

** Visuals

Color theme.

#+BEGIN_SRC emacs-lisp
(load-theme 'deeper-blue)
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark))
#+END_SRC

Cursor color

#+BEGIN_SRC emacs-lisp
(set-cursor-color "lightsalmon1")
(set-mouse-color "white")
#+END_SRC

Font

#+BEGIN_SRC emacs-lisp
(set-face-attribute 'default nil :font "Source Code Pro 11")
;; (setq default-frame-alist '((font . "Inconsolata LGC 14")))
(setq-default line-spacing 0)
(setq initial-frame-alist '((width . 135) (height . 55)))
(tool-bar-mode -1)
#+END_SRC

Matching parenthesis appearance

#+BEGIN_SRC emacs-lisp
(set-face-background 'show-paren-match "salmon3")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)
(show-paren-mode)
#+END_SRC

Wrap lines always.

#+BEGIN_SRC emacs-lisp
;; (global-visual-line-mode 1)
(setq org-startup-truncated nil)
#+END_SRC

Nice and simple mode line.

#+BEGIN_SRC emacs-lisp
(setq column-number-mode t) ;; show columns in addition to rows in mode line
#+END_SRC

Show full path in the title bar.

#+BEGIN_SRC emacs-lisp
(setq-default frame-title-format "%b (%f)")
#+END_SRC

Never use tabs, use spaces instead.

#+BEGIN_SRC emacs-lisp
(setq-default indent-tabs-mode nil)
(setq tab-width 2)
(setq js-indent-level 2)
(setq css-indent-offset 2)
(setq-default c-basic-offset 2)
(setq c-basic-offset 2)
(setq-default tab-width 2)
(setq-default c-basic-indent 2)
#+END_SRC

Disable blinking cursor.
Disable suspending on =C-z=.

#+BEGIN_SRC emacs-lisp
(use-package frame
 :ensure nil
 :config
 (blink-cursor-mode 0)
 :bind
 ("C-z" . nil))
#+END_SRC

=C-c C-g= always quits minibuffer.

#+BEGIN_SRC emacs-lisp
(use-package delsel
 :ensure nil
 :bind
 ("C-c C-g" . minibuffer-keyboard-quit))
#+END_SRC

Visual lines.

#+BEGIN_SRC emacs-lisp
(global-visual-line-mode t)
#+END_SRC

** Sane defaults

I don't care about auto save and backup files.

#+BEGIN_SRC emacs-lisp
(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil)  ; stop creating .# files
#+END_SRC

Alarms off

#+BEGIN_SRC emacs-lisp
(setq ring-bell-function 'ignore)  ; turn off alarms completely
#+END_SRC

Line numbers on

#+BEGIN_SRC emacs-lisp
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))
#+END_SRC

Dialog boxes

#+BEGIN_SRC emacs-lisp
(setq
 use-dialog-box nil       ; Disable dialog boxes
 use-file-dialog nil)     ; Disable file dialog
#+END_SRC

Scroll

#+BEGIN_SRC emacs-lisp
(setq
 scroll-margin 0
 scroll-conservatively 0
 scroll-preserve-screen-position nil
 scroll-step 1)
#+END_SRC

** UI

Hide toolbar and scrollbars.

#+BEGIN_SRC emacs-lisp
(tool-bar-mode -1)
(scroll-bar-mode -1)
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))
#+END_SRC

I generally prefer to hide the menu bar, but doing this on OS X simply makes it
update unreliably in GUI frames, so we make an exception.

#+BEGIN_SRC emacs-lisp
(if (eq system-type 'darwin)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (set-frame-parameter frame 'menu-bar-lines
                                     (if (display-graphic-p frame) 1 0))))
  (when (fboundp 'menu-bar-mode)
    (menu-bar-mode -1)))
#+END_SRC

Disable [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Bidirectional-Display.html][bidirectional text]] for tiny performance boost.

#+BEGIN_SRC emacs-lisp
(setq-default bidi-display-reordering nil)
#+END_SRC

Don't [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Blinking.html][blink matching paren]], it's too distracting.

#+BEGIN_SRC emacs-lisp
(setq-default blink-matching-paren nil)
#+END_SRC

*** Cursor

Hide cursor in inactive windows.

#+BEGIN_SRC emacs-lisp
(setq-default cursor-in-non-selected-windows nil)
#+END_SRC

- Display vertical bar cursor with default width.
- Draw block cursor as wide as the glyph under it.

#+BEGIN_SRC emacs-lisp
(setq-default
 cursor-type 'bar
 x-stretch-cursor t)
#+END_SRC

Show full path in the title bar.

#+BEGIN_SRC emacs-lisp
(setq-default frame-title-format "%b (%f)")
#+END_SRC

Don't implicitly resize the frame's display area in order to preserve the number of columns or lines the frame displays when changing font, menu bar, tool bar, internal borders, fringes or scroll bars.
Read [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Implied-Frame-Resizing.html][this]] for more info.

#+BEGIN_SRC emacs-lisp
(setq-default frame-inhibit-implied-resize t)
#+END_SRC

Hide the =window-divider= (a line separating windows).

#+BEGIN_SRC emacs-lisp
(when (boundp 'window-divider-mode)
  (setq window-divider-default-places t
        window-divider-default-bottom-width 0
        window-divider-default-right-width 0)
  (window-divider-mode +1))
#+END_SRC

Line-spacing
Non-zero values for =line-spacing= can mess up ansi-term and co, so we
zero it explicitly in those cases.

#+BEGIN_SRC emacs-lisp
(add-hook
 'term-mode-hook
 (lambda () (setq line-spacing 0)))
#+END_SRC

Highlight parens.

#+BEGIN_SRC emacs-lisp
(setq show-paren-style 'parenthesis)
(show-paren-mode 1)
#+END_SRC

Treat an Emacs region much like a typical text selection outside of Emacs.

#+BEGIN_SRC emacs-lisp
(setq delete-selection-mode t)
#+END_SRC

Set left and right margins for every window.

#+BEGIN_SRC emacs-lisp
(setq-default
 left-margin-width 2
 right-margin-width 2)
#+END_SRC

** Time

Display time

#+BEGIN_SRC emacs-lisp
(use-package time
 :ensure nil
 :custom
 (display-time-default-load-average nil)
 (display-time-24hr-format t)
 :config
 (display-time-mode t))
#+END_SRC

** Warnings

Decrease the =obsolete= warnings annoyance level.

#+BEGIN_SRC emacs-lisp
(setq byte-compile-warnings '(not obsolete))
#+END_SRC

This helps to get rid of =functions might not be defined at runtime= warnings.
See [[https://github.com/jwiegley/use-package/issues/590][this issue]] for details.

#+BEGIN_SRC emacs-lisp
(eval-when-compile
  (setq use-package-expand-minimally byte-compile-current-file))
#+END_SRC

Suppress ~ad-handle-definition~ warnings.

#+BEGIN_SRC emacs-lisp
(setq ad-redefinition-action 'accept)
#+END_SRC

Revert (update) buffers automatically when underlying files are changed externally.

#+BEGIN_SRC emacs-lisp
(global-auto-revert-mode t)
#+END_SRC

Some basic things.

#+BEGIN_SRC emacs-lisp
(setq
 inhibit-startup-message t         ; Don't show the startup message
 inhibit-startup-screen t          ; or screen
 cursor-in-non-selected-windows t  ; Hide the cursor in inactive windows

 echo-keystrokes 0.1               ; Show keystrokes right away, don't show the message in the scratch buffer
 initial-scratch-message nil       ; Empty scratch buffer
 sentence-end-double-space nil     ; Sentences should end in one space, come on!
)

(fset 'yes-or-no-p 'y-or-n-p)      ; y and n instead of yes and no everywhere else
(scroll-bar-mode -1)
(delete-selection-mode 1)
(global-unset-key (kbd "s-p"))
#+END_SRC

I want Emacs kill ring and system clipboard to be independent. Simpleclip is the solution to that.

#+BEGIN_SRC emacs-lisp
(use-package simpleclip
  :config
  (simpleclip-mode 1))
#+END_SRC

** OS integration

Pass system shell environment to Emacs. This is important primarily for shell inside Emacs, but also things like Org mode export to Tex PDF don't work, since it relies on running external command =pdflatex=, which is loaded from =PATH=.

#+BEGIN_SRC emacs-lisp
(use-package exec-path-from-shell)

(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))
#+END_SRC

A nice little real terminal in a popup.

#+BEGIN_SRC emacs-lisp
(use-package shell-pop)
#+END_SRC

** Navigation and editing

Kill line with =s-Backspace=, which is =Cmd-Backspace=. Note that thanks to Simpleclip, killing doesn't rewrite the system clipboard. Kill one word by =Alt-Backspace=. Also, kill forward word with =Alt-Shift-Backspace=, since =Alt-Backspace= is kill word backwards.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "s-<backspace>") 'kill-whole-line)
(global-set-key (kbd "M-S-<backspace>") 'kill-word)
#+END_SRC

Use =super= (which is =Cmd=) for movement and selection just like in macOS.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "s-<right>") 'end-of-visual-line)
(global-set-key (kbd "s-<left>") 'beginning-of-visual-line)
;; (global-set-key (kbd "S-s-<left>") (kbd "M-S-m"))

(global-set-key (kbd "s-<up>") 'beginning-of-buffer)
(global-set-key (kbd "s-<down>") 'end-of-buffer)

(global-set-key (kbd "s-l") 'goto-line)
#+END_SRC

Basic things you should expect from macOS.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "s-a") 'mark-whole-buffer)       ;; select all
(global-set-key (kbd "s-s") 'save-buffer)             ;; save
(global-set-key (kbd "s-S") 'write-file)              ;; save as
(global-set-key (kbd "s-q") 'save-buffers-kill-emacs) ;; quit
(global-set-key (kbd "s-z") 'undo)                    ;; undo
#+END_SRC

Avy for fast navigation.

#+BEGIN_SRC emacs-lisp
(use-package avy
  :config
  (global-set-key (kbd "s-;") 'avy-goto-char-timer))
#+END_SRC

Go back to previous mark (position) within buffer and go back (forward?).

#+BEGIN_SRC emacs-lisp
(defun my-pop-local-mark-ring ()
  (interactive)
  (set-mark-command t))

(defun unpop-to-mark-command ()
  "Unpop off mark ring. Does nothing if mark ring is empty."
  (interactive)
      (when mark-ring
        (setq mark-ring (cons (copy-marker (mark-marker)) mark-ring))
        (set-marker (mark-marker) (car (last mark-ring)) (current-buffer))
        (when (null (mark t)) (ding))
        (setq mark-ring (nbutlast mark-ring))
        (goto-char (marker-position (car (last mark-ring))))))

(global-set-key (kbd "s-,") 'my-pop-local-mark-ring)
(global-set-key (kbd "s-.") 'unpop-to-mark-command)
#+END_SRC

Since =Cmd+,= and =Cmd+.= move you back in forward in the current buffer, the same keys with =Shift= move you back and forward between open buffers.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "s-<") 'previous-buffer)
(global-set-key (kbd "s->") 'next-buffer)
#+END_SRC

Go to other windows easily with one keystroke =s-something= instead of =C-x something=.

#+BEGIN_SRC emacs-lisp
(defun vsplit-last-buffer ()
  (interactive)
  (split-window-vertically)
  (other-window 1 nil)
  (switch-to-next-buffer))

(defun hsplit-last-buffer ()
  (interactive)
  (split-window-horizontally)
  (other-window 1 nil)
  (switch-to-next-buffer))

(global-set-key (kbd "s-w") (kbd "C-x 0")) ;; just like close tab in a web browser
(global-set-key (kbd "s-W") (kbd "C-x 1")) ;; close others with shift

(global-set-key (kbd "s-T") 'vsplit-last-buffer)
(global-set-key (kbd "s-t") 'hsplit-last-buffer)
#+END_SRC

Expand-region allows to gradually expand selection inside words, sentences, etc. =C-'= is bound to Org's =cycle through agenda files=, which I don't really use, so I unbind it here before assigning global shortcut for expansion.

#+BEGIN_SRC emacs-lisp
(use-package expand-region
  :config
  (global-set-key (kbd "s-'") 'er/expand-region)
  (global-set-key (kbd "s-\"") 'er/contract-region))

#+END_SRC

=Move-text= allows moving lines around with meta-up/down.

#+BEGIN_SRC emacs-lisp
(use-package move-text
  :config
  (move-text-default-bindings))
#+END_SRC

Smarter open-line by [[http://emacsredux.com/blog/2013/03/26/smarter-open-line/][bbatsov]]. Once again, I'm taking advantage of CMD and using it to quickly insert new lines above or below the current line, with correct indentation and stuff.

#+BEGIN_SRC emacs-lisp
(defun smart-open-line ()
  "Insert an empty line after the current line. Position the cursor at its beginning, according to the current mode."
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent))

(defun smart-open-line-above ()
  "Insert an empty line above the current line. Position the cursor at it's beginning, according to the current mode."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key (kbd "s-<return>") 'smart-open-line)
(global-set-key (kbd "s-S-<return>") 'smart-open-line-above)
#+END_SRC

Join lines whether you're in a region or not.

#+BEGIN_SRC emacs-lisp
(defun smart-join-line (beg end)
  "If in a region, join all the lines in it. If not, join the current line with the next line."
  (interactive "r")
  (if mark-active
      (join-region beg end)
      (top-join-line)))

(defun top-join-line ()
  "Join the current line with the next line."
  (interactive)
  (delete-indentation 1))

(defun join-region (beg end)
  "Join all the lines in the region."
  (interactive "r")
  (if mark-active
      (let ((beg (region-beginning))
            (end (copy-marker (region-end))))
        (goto-char beg)
        (while (< (point) end)
          (join-line 1)))))

(global-set-key (kbd "s-j") 'smart-join-line)
#+END_SRC

Upcase word and region using the same keys.

#+Begin_SRC emacs-lisp
(global-set-key (kbd "M-u") 'upcase-dwim)
(global-set-key (kbd "M-l") 'downcase-dwim)
#+END_SRC

Provide nice visual feedback for replace.

#+BEGIN_SRC emacs-lisp
(use-package visual-regexp
  :config
  (define-key global-map (kbd "s-r") 'vr/replace))
#+END_SRC

Delete trailing spaces and add new line in the end of a file on save.

#+BEGIN_SRC emacs-lisp
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq require-final-newline t)
#+END_SRC

Multiple cusors are a must. Make <return> insert a newline; multiple-cursors-mode can still be disabled with C-g.

#+BEGIN_SRC emacs-lisp
(use-package multiple-cursors
  :config
  (setq mc/always-run-for-all 1)
  (global-set-key (kbd "s-d") 'mc/mark-next-like-this)
  (global-set-key (kbd "s-D") 'mc/mark-all-dwim)
  (define-key mc/keymap (kbd "<return>") nil))
#+END_SRC

Comment lines.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "s-/") 'comment-line)
#+END_SRC

** Windows

I'm still not happy with the way new windows are spawned. For now, at least, let's make it so that new automatic windows are always created on the bottom, not on the side.

#+BEGIN_SRC emacs-lisp
(setq split-height-threshold 0)
(setq split-width-threshold nil)
#+END_SRC

Move between windows with Control-Command-Arrow and with =Cmd= just like in iTerm.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "s-o") (kbd "C-x o"))

(use-package windmove
  :config
  (global-set-key (kbd "s-[")  'windmove-left)         ;; Cmd+[ go to left window
  (global-set-key (kbd "s-]")  'windmove-right)        ;; Cmd+] go to right window
  (global-set-key (kbd "s-{")  'windmove-up)           ;; Cmd+Shift+[ go to upper window
  (global-set-key (kbd "s-}")  'windmove-down))      ;; Ctrl+Shift+[ go to down window
#+END_SRC

Enable winner mode to quickly restore window configurations

#+BEGIN_SRC emacs-lisp
(winner-mode 1)
#+END_SRC

Shackle to make sure all windows are nicely positioned.

#+BEGIN_SRC emacs-lisp
(use-package shackle
  :init
  (setq shackle-default-alignment 'below
        shackle-default-size 0.4
        shackle-rules '((help-mode           :align below :select t)
                        (helpful-mode        :align below)
                        (compilation-mode    :select t   :size 0.25)
                        ("*compilation*"     :select nil :size 0.25)
                        ("*ag search*"       :select nil :size 0.25)
                        ("*Flycheck errors*" :select nil :size 0.25)
                        ("*Warnings*"        :select nil :size 0.25)
                        ("*Error*"           :select nil :size 0.25)
                        ("*Org Links*"       :select nil :size 0.1)
                        (magit-status-mode                :align bottom :size 0.5  :inhibit-window-quit t)
                        (magit-log-mode                   :same t                  :inhibit-window-quit t)
                        (magit-commit-mode                :ignore t)
                        (magit-diff-mode     :select nil  :align left   :size 0.5)
                        (git-commit-mode                  :same t)
                        (vc-annotate-mode                 :same t)
                        ))
  :config
  (shackle-mode 1))
#+END_SRC

** Edit indirect

Select any region and edit it in another buffer.

#+BEGIN_SRC emacs-lisp
(use-package edit-indirect)
#+END_SRC

** Git

It's time for Magit!

#+BEGIN_SRC emacs-lisp
(use-package magit
  :config
  (global-set-key (kbd "s-g") 'magit-status))
#+END_SRC

Navigate to projects with Cmd+Shift+P (thanks to reddit user and emacscast listener fritzgrabo).

#+BEGIN_SRC emacs-lisp
(setq magit-repository-directories '(("\~/code" . 4) ("\~/Google Drive/Codexpanse/Course materials" . 3)))

(defun magit-status-with-prefix-arg ()
  "Call `magit-status` with a prefix."
  (interactive)
  (let ((current-prefix-arg '(4)))
    (call-interactively #'magit-status)))

(global-set-key (kbd "s-P") 'magit-status-with-prefix-arg)
#+END_SRC

** Auto completion

#+BEGIN_SRC emacs-lisp
(use-package company
  :config
  (setq company-idle-delay 0.1)
  (setq company-global-modes '(not org-mode markdown-mode))
  (setq company-minimum-prefix-length 1)
  (add-hook 'after-init-hook 'global-company-mode))
#+END_SRC

** Frames, windows, buffers

Always open files in the same frame, even when double-clicked from Finder.

#+BEGIN_SRC emacs-lisp
(setq ns-pop-up-frames nil)
#+END_SRC

Handy killall command.

#+BEGIN_SRC emacs-lisp
(defun kill-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))
#+END_SRC

** Customizations

Store custom-file separately, don't freak out when it's not found.

#+BEGIN_SRC emacs-lisp
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)
#+END_SRC

** Manually installed packages

#+BEGIN_SRC emacs-lisp :tangle no
(add-to-list 'load-path "~/.emacs.d/lisp/")
(load "edit-indirect")
#+END_SRC

** Which key

This is great for learning Emacs, it shows a nice table of possible commands.

#+BEGIN_SRC emacs-lisp :tangle no
(use-package which-key
  :init
  (setq
    which-key-idle-delay 0.5
    which-key-sort-order 'which-key-prefix-then-key-order-reverse
    which-key-prefix-prefix ""
    which-key-side-window-max-width 0.5
    which-key-popup-type 'side-window
    which-key-side-window-location 'bottom)
  :config
  (which-key-mode))
#+END_SRC

** More

On save/write file:
- Automatically delete trailing whitespace.
- Silently put a newline at the end of file if there isn't already one there.

#+BEGIN_SRC emacs-lisp
(use-package files
 :ensure nil
 :commands
 (generate-new-buffer
  executable-find
  file-name-base
  file-name-extension)
 :custom
 (require-final-newline t)
 :hook
 (before-save . delete-trailing-whitespace))
#+END_SRC

Diminish [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Autorevert.html#Autorevert][autorevert]] mode.

#+BEGIN_SRC emacs-lisp
(use-package autorevert
 :ensure nil
 :custom
 ;; Don't generate any messages whenever a buffer is reverted
 (auto-revert-verbose nil)
 ;; Operate only on file-visiting buffers
 (global-auto-revert-non-file-buffers t)
 :diminish auto-revert-mode)
#+END_SRC

Automatically save place in each file.

#+BEGIN_SRC emacs-lisp
(setq
 save-place-forget-unreadable-files t
 save-place-limit 200)

(save-place-mode 1)
#+END_SRC

<C-tab> to change window

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "<C-tab>") 'other-window)
#+END_SRC

Dired settings

#+BEGIN_SRC emacs-lisp
(define-key dired-mode-map [mouse-1] 'dired-find-file)
#+END_SRC


* Quelpa

Setup [[https://framagit.org/steckerhalter/quelpa][quelpa]].

#+BEGIN_SRC emacs-lisp
(if (require 'quelpa nil t)
  ;; Prevent quelpa from doing anyting that requires network connection.
  (setq
   quelpa-update-melpa-p nil    ; Don't update MELPA git repo
   quelpa-checkout-melpa-p nil  ; Don't clone MELPA git repo
   quelpa-upgrade-p nil         ; Don't try to update packages automatically
   quelpa-self-upgrade-p nil)   ; Don't upgrade quelpa automatically

  ;; Comment/uncomment line below to disable/enable quelpa auto-upgrade.
  ;; (quelpa-self-upgrade)

  (with-temp-buffer
    (url-insert-file-contents "https://raw.github.com/quelpa/quelpa/master/bootstrap.el")
    (eval-buffer)))
#+END_SRC

Install =use-package= and the =quelpa= handler.

#+BEGIN_SRC emacs-lisp
(quelpa
 '(quelpa-use-package
   :fetcher github
   :repo "quelpa/quelpa-use-package"))
(require 'quelpa-use-package)
#+END_SRC

Advice setting ~:ensure nil~ for =use-package= + =quelpa=.

#+BEGIN_SRC emacs-lisp
(quelpa-use-package-activate-advice)
#+END_SRC


* Org

Allow shift selection with arrows. This will not interfere with some built-in shift+arrow functionality in Org.

#+BEGIN_SRC emacs-lisp
(setq org-support-shift-select t)
#+END_SRC


* Flycheck
*** Mode

On-the-fly syntax checking for GNU Emacs.
See the [[https://www.flycheck.org/en/latest/index.html][flycheck.org]] for more info.

#+BEGIN_SRC emacs-lisp
(use-package flycheck
  :after (general)
  :demand t
  :commands
  (global-flycheck-mode)
  :init
  (setq-default
   flycheck-disabled-checkers
   '(emacs-lisp-checkdoc
     javascript-jshint
     haskell-stack-ghc
     haskell-ghc
     haskell-hlint))
  (setq
   flycheck-highlighting-mode 'lines
   flycheck-indication-mode 'left-fringe
   flycheck-mode-line-prefix "fly"
   flycheck-javascript-eslint-executable "eslint_d")
  :config
  (global-flycheck-mode 1)
  ;; Make the error list display like similar lists in contemporary IDEs
  ;; like VisualStudio, Eclipse, etc.
  (add-to-list
   'display-buffer-alist
   `(,(rx bos "*errors*" eos)
     ;; (display-buffer-reuse-window
     ;;  display-buffer-in-side-window)
     (side . bottom)
     (reusable-frames . visible)
     (window-height . 0.33)))
  (unbind-key "C-j" flycheck-error-list-mode-map)
  :diminish flycheck-mode)
#+END_SRC

*** Popup (flycheck-popup-tip)

 Display Flycheck error messages using =popup.el=.
 Can be used together with the =flyheck-pos-tip=.

#+BEGIN_SRC emacs-lisp :tangle no
(use-package flycheck-popup-tip
  :after (flycheck)
  :custom
  (flycheck-popup-tip-error-prefix "* ")
  :config
  (flycheck-popup-tip-mode))
#+END_SRC

*** Popup (flycheck-pos-tip)

Displays flycheck errors in tooltip. However, it does not
display popup if you run Emacs under TTY. It displays message on
echo area and that is often used for ELDoc. Also, popups made by
pos-tip library does not always look good, especially on macOS
and Windows.

Can be used together with the =flyheck-popup-tip=.

#+BEGIN_SRC emacs-lisp :tangle no
(use-package flycheck-pos-tip
  :after (flycheck flycheck-popup-tip)
  :commands
  (flycheck-pos-tip-error-messages)
  :config
  (setq
   flycheck-pos-tip-display-errors-tty-function
   #'flycheck-popup-tip-show-popup)
  (flycheck-pos-tip-mode))
#+END_SRC


* Coding
** Languages
*** Lisp

#+BEGIN_SRC emacs-lisp
(use-package lisp-mode
  :ensure nil
  :config
  (put 'use-package 'lisp-indent-function 1)
  (put 'add-hook 'lisp-indent-function 1))
#+END_SRC

*** Emacs Lisp
**** Mode

#+BEGIN_SRC emacs-lisp
(use-package elisp-mode
 :after (general company smartparens)
 :ensure nil
 :config
 (add-to-list 'company-backends 'company-elisp)
 (sp-with-modes 'emacs-lisp-mode
   (sp-local-pair "'" nil :actions nil)))
#+END_SRC

*** Nix
**** Mode

An emacs major mode for editing nix expressions.

#+BEGIN_SRC emacs-lisp
(use-package nix-mode
  :after (general)
  :mode ("\\.nix\\'" "\\.nix.in\\'")
  :config
  :delight "nix")
#+END_SRC

**** Drv mode

A major mode for viewing Nix derivations (=.drv= files).

#+BEGIN_SRC emacs-lisp
(use-package nix-drv-mode
  :ensure nix-mode
  :mode "\\.drv\\'")
#+END_SRC

**** Shell (disabled)

#+BEGIN_SRC emacs-lisp :tangle no
(use-package nix-shell
  :ensure nix-mode
  :commands (nix-shell-unpack nix-shell-configure nix-shell-build))
#+END_SRC

**** REPL

#+BEGIN_SRC emacs-lisp
(use-package nix-repl
  :ensure nix-mode
  :commands (nix-repl))
#+END_SRC

**** Company

Company backend for NixOS options.

#+BEGIN_SRC emacs-lisp
(use-package company-nixos-options
  :after (company)
  :commands (company-nixos-options)
  :config
  (add-to-list 'company-backends 'company-nixos-options))
#+END_SRC

**** Sandbox

#+BEGIN_SRC emacs-lisp
(use-package nix-sandbox)
#+END_SRC

**** Flycheck

TODO: https://github.com/travisbhartwell/nix-emacs#flycheck

*** Ocaml

Nix(OS)-specific.
The main idea is [[https://blog.jethro.dev/blog/ocaml_with_nix/][described here.]]

**** Variables

#+BEGIN_SRC emacs-lisp
(defconst my/merlin-site-elisp (getenv "MERLIN_SITE_LISP"))
(defconst my/utop-site-elisp (getenv "UTOP_SITE_LISP"))
(defconst my/ocp-site-elisp (getenv "OCP_INDENT_SITE_LISP"))
(defun my/ocaml-in-emacs-p ()
  (string-equal (getenv "OCAML_IN_EMACS") "1"))
#+END_SRC

**** Tuareg

[[https://github.com/ocaml/tuareg][Tuareg]] is an Emacs OCaml mode.

Provides:
- Syntax highlighting.
- REPL (aka =toplevel=).
- OCaml debugger within Emacs.

Usage:
- Start the OCaml REPL with ~M-x run-ocaml~.
- Run the OCaml debugger with ~M-x ocamldebug FILE~.

#+BEGIN_SRC emacs-lisp
(use-package tuareg
  :demand t
  :mode
  (("\\.ml[ily]?$" . tuareg-mode)
   ("\\.topml$" . tuareg-mode))
  :init
  (setq tuareg-match-patterns-aligned t)
  :hook
  (caml-mode . tuareg-mode)
  :delight "ocaml")
#+END_SRC

**** general-define-key

#+BEGIN_SRC emacs-lisp
(use-package general)
#+END_SRC


**** Smartparens

#+BEGIN_SRC emacs-lisp
(use-package smartparens
 :commands
 (smartparens-mode
  sp-with-modes
  sp-local-pair
  sp-pair)
 :hook
 ((conf-mode text-mode prog-mode) . smartparens-mode)
 :config
 (use-package smartparens-config
  :ensure nil
  :demand t)
 (general-define-key
 :diminish smartparens-mode))
#+END_SRC


**** Merlin

Context sensitive completion for OCaml. Provides modern IDE features.
Implements a minor-mode that is supposed to be used on top of =tuareg-mode=.

See the [[https://github.com/ocaml/merlin][package repo]] for more info.

#+BEGIN_SRC emacs-lisp
(use-package merlin
  :after (company tuareg)
  :if (and my/merlin-site-elisp (my/ocaml-in-emacs-p))
  :load-path my/merlin-site-elisp
  :init
  ;; Disable merlin's own error checking
  ;; We'll use flycheck-ocaml for that
  (setq
   merlin-error-after-save nil
   merlin-completion-with-doc t
   merlin-command "/run/current-system/sw/bin/ocamlmerlin")
  :config
  (add-to-list 'company-backends 'merlin-company-backend)
  :hook
  ((tuareg-mode caml-mode) . merlin-mode))
#+END_SRC

**** Merlin eldoc

Automatically (without using keybindings) provide information for the value
under point in OCaml and ReasonML files.

#+BEGIN_SRC emacs-lisp
(use-package merlin-eldoc
 :after (merlin)
 :custom
 (eldoc-echo-area-use-multiline-p t)   ; Use multiple lines when necessary
 (merlin-eldoc-max-lines 8)            ; But not more than 8
 (merlin-eldoc-type-verbosity 'min)    ; Don't display verbose types
 (merlin-eldoc-function-arguments nil) ; Don't show function arguments
 (merlin-eldoc-doc nil)                ; Don't show the documentation
 :config
 :hook
 ((tuareg-mode reason-mode) . merlin-eldoc-setup))
#+END_SRC

**** Utop

https://github.com/diml/utop#integration-with-emacs

#+BEGIN_SRC emacs-lisp
(use-package utop
  :after (tuareg)
  :if (and my/utop-site-elisp (my/ocaml-in-emacs-p))
  :load-path my/utop-site-elisp
  :commands
  (utop-command)
  :config
  (setq utop-command "opam config exec utop -- -emacs")
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
  :hook
  ((tuareg-mode reason-mode) . utop-minor-mode))
#+END_SRC

**** Indentation

#+BEGIN_SRC emacs-lisp
(use-package ocp-indent
  :after (tuareg)
  :if (and my/ocp-site-elisp (my/ocaml-in-emacs-p))
  :load-path my/ocp-site-elisp
  :config)
#+END_SRC

**** Dune

A [[https://github.com/ocaml/dune][composable build system]] for OCaml.

#+BEGIN_SRC emacs-lisp
(use-package dune)
#+END_SRC

**** Flycheck

OCaml support for Flycheck using Merlin.

#+BEGIN_SRC emacs-lisp
(use-package flycheck-ocaml
 :after (flycheck merlin)
 :demand t
 :commands
 (flycheck-ocaml-setup)
 :config
 ;; Enable flycheck checker
 (flycheck-ocaml-setup))
#+END_SRC

*** ReasonML

#+BEGIN_SRC emacs-lisp
(use-package reason-mode
 :quelpa
 (reason-mode :repo "reasonml-editor/reason-mode" :fetcher github :stable t)
 :config
 (add-hook
  'reason-mode-hook
  (lambda ()
    (setq utop-command "opam config exec -- rtop -emacs")
    (add-hook 'before-save-hook 'refmt-before-save)))
    (add-hook 'reason-mode-hook 'merlin-mode)
    (add-hook 'reason-mode-hook 'utop-minor-mode)
    (add-hook 'reason-mode-hook 'flycheck-mode)
    :delight "re")
#+END_SRC
*** Rust

#+BEGIN_SRC emacs-lisp
(use-package rust-mode
 :after (company general)
 :if (executable-find "rustc")
 :commands (rust-mode)
 :config
 ;; enable rust-mode for .lalrpop files
 (add-to-list 'auto-mode-alist '("\\.lalrpop\\'" . rust-mode))
 (general-define-key
  :keymaps 'rust-mode-map
  "TAB" 'company-indent-or-complete-common
  "C-c C-b" 'rust-compile
  "C-c <tab>" 'rust-format-buffer))
#+END_SRC

Some of key bindings are provided by ==evil-collection=.

#+BEGIN_SRC emacs-lisp
(use-package racer
 :after rust-mode
 :demand t
 :commands racer-mode
 :hook
 ((racer-mode . eldoc-mode)
  (racer-mode . company-mode)
  (rust-mode . racer-mode)))
#+END_SRC

#+BEGIN_SRC emacs-lisp
(use-package company-racer
 :after (racer company)
 :config
 (add-to-list 'company-backends 'company-racer))
#+END_SRC

#+BEGIN_SRC emacs-lisp
(use-package flycheck-rust
  :config
  (with-eval-after-load 'rust-mode
    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)))
#+END_SRC
